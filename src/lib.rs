#![feature(async_await, await_macro, futures_api)]

pub mod client;
pub mod model;

mod error;

pub use client::Client;
pub use error::{Error, Result};
