use cache::Cache;
use crate::{
    error::Result,
    model::Authorization,
};
use discord_bots_org::{
    model::ShardStats as DiscordBotsOrgStats,
    ReqwestAsyncClient as DiscordBotsOrg,
};
use futures::compat::Future01CompatExt;
use futures_locks::RwLock;
use log::{debug, warn};
use reqwest::r#async::Client as ReqwestClient;
use std::{
    sync::Arc,
    time::{Duration, Instant},
};

#[derive(Debug, Default)]
struct Clients {
    discord_bots_org: Option<(String, DiscordBotsOrg)>,
}

pub struct Client {
    cache: Arc<Cache>,
    clients: Clients,
    discord_user_id: u64,
    // Used to keep track of when the last time an update was sent out was.
    last: RwLock<Instant>,
}

impl Client {
    pub fn new(
        auth: Authorization,
        discord_user_id: u64,
        inner: Arc<ReqwestClient>,
        cache: Arc<Cache>,
    ) -> Self {
        let mut clients = Clients::default();

        if let Some(auth) = auth.discord_bots_org {
            let client = DiscordBotsOrg::new(Arc::clone(&inner));

            clients.discord_bots_org = Some((auth, client));
        }

        Self {
            last: RwLock::new(Instant::now() - Duration::from_secs(10)),
            cache,
            clients,
            discord_user_id,
        }
    }

    pub async fn update(&self) -> Result<()> {
        // Only update every 10 seconds, at most.
        //
        // First, wait for a read lock. If 10 seconds haven't elapsed, return.
        //
        // RAII drops the lock so other futures can also read, and so that our
        // later write guard doesn't block infinitely.
        match await!(self.last.read().compat()) {
            Ok(reader) => {
                if reader.elapsed() < Duration::from_secs(1) {
                    return Ok(());
                }
            },
            Err(()) => {
                warn!("Error unlocking bot list timer");

                return Ok(());
            },
        }

        // If it's been over 10 seconds, acquire a write lock and update the
        // last instant time to now.
        //
        // Note that between the actual acquire of the read lock and the new
        // acquire of the write lock, the current thread's event loop has not
        // switched asynchronous context. However, another event loop in the
        // ThreadPool _may_ have already gotten here. We're willing to accept
        // this risk at a performance gain.
        match await!(self.last.write().compat()) {
            Ok(mut writer) => {
                *writer = Instant::now();
            },
            Err(()) => {
                warn!("Error acquiring write lock");

                // This probably doesn't happen, and it's probably safe to
                // continue on and post to APIs, but "just in case", let's not.
                return Ok(());
            },
        }


        let count = await!(self.cache.guild_count())?;
        debug!("New guild count: {}", count);

        if let Some((auth, client)) = self.clients.discord_bots_org.as_ref() {
            await!(self.discord_bots_org(count as u64, client, &auth))?;
        }

        Ok(())
    }

    async fn discord_bots_org<'a>(
        &'a self,
        guild_count: u64,
        client: &'a DiscordBotsOrg,
        auth: &'a str,
    ) -> Result<()> {
        let stats = DiscordBotsOrgStats::Cumulative {
            shard_count: None,
            guild_count,
        };

        await!(client.post_stats(
            auth,
            self.discord_user_id,
            &stats,
        )).map_err(From::from)
    }
}
