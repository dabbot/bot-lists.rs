use cache::Error as CacheError;
use discord_bots_org::Error as DiscordBotsOrgError;
use std::{
    error::Error as StdError,
    fmt::{Display, Formatter, Result as FmtResult},
    result::Result as StdResult,
};

pub type Result<T> = StdResult<T, Error>;

#[derive(Debug)]
pub enum Error {
    Cache(CacheError),
    DiscordBotsOrg(DiscordBotsOrgError),
}

impl From<CacheError> for Error {
    fn from(e: CacheError) -> Self {
        Error::Cache(e)
    }
}

impl From<DiscordBotsOrgError> for Error {
    fn from(e: DiscordBotsOrgError) -> Self {
        Error::DiscordBotsOrg(e)
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        f.write_str(self.description())
    }
}

impl StdError for Error {
    fn description(&self) -> &str {
        use Error::*;

        match self {
            Cache(e) => e.description(),
            DiscordBotsOrg(e) => e.description(),
        }
    }
}
